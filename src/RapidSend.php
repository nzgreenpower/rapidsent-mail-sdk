<?php

namespace RapidSend;

use Buzz\Browser;
use Buzz\Client\Curl;

class RapidSend
{
    protected $endpoint;
    protected $apiKey;
    protected $apiAuth;
    protected $mailApiKey;
    protected $subscribeListApiKey;

    public function __construct($endpoint, $apiKey, $apiAuth)
    {
        $this->endpoint = $endpoint;
        $this->apiKey = $apiKey;
        $this->apiAuth = $apiAuth;
    }

    public function setMailApiKey($mailApiKey){
        $this->mailApiKey = $mailApiKey;
    }

    public function setSubscribeListApiKey($subscribeListApiKey){
        $this->subscribeListApiKey = $subscribeListApiKey;
    }

    public function send($parameters,$timeout=5){
        // Create Curl Client
        $curl = new Curl();
        $curl->setTimeout($timeout);
        $browser = new Browser();
        $browser->setClient($curl);
        $header = array(
            'Accept' => 'application/json',
            'Accept-Charset' => 'iso-8859-1,*,utf-8',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept-Language' => 'en-US',
            'Api-key'=>$this->apiKey,
            'Auth-key'=>$this->apiAuth
        );

        if (!$this->mailApiKey){
            return false;
        }

        $mailApiArray = array('transaction_api_key'=>$this->mailApiKey);
        $parameters = array_merge($mailApiArray,$parameters);

        $response = $browser->submit($this->endpoint,$parameters,'POST',$header);
        $result =$response->getContent();
        if ($result = json_decode($result)){
            if (!isset($result->success)){
                echo $result->error;
                die;
            }else{
                return $result->success;
            }

        }
        return false;
    }

}